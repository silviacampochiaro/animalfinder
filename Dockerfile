FROM openjdk:8-jdk-alpine
USER root
COPY target/AnimalFinder-0.0.1-SNAPSHOT/WEB-INF/lib /app/lib
COPY target/AnimalFinder-0.0.1-SNAPSHOT/META-INF /app/META-INF
COPY target/AnimalFinder-0.0.1-SNAPSHOT/WEB-INF/classes /app
COPY target/AnimalFinder-0.0.1-SNAPSHOT/WEB-INF/classes/log4j2.xml /src/main/resources/
ENTRYPOINT ["java","-cp","app:app/lib/*","com.demo.animalfinder.AnimalFinderApplication"]