package com.demo.animalfinder.service;

import com.demo.animalfinder.controller.AnimalController;
import com.demo.animalfinder.exceptions.AnimalException;
import com.demo.animalfinder.exceptions.Errors;
import com.demo.animalfinder.model.Animal;
import com.demo.animalfinder.repository.AnimalDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AnimalService {


    private static final Logger LOGGER = LoggerFactory.getLogger(AnimalController.class);

    @Autowired
    private AnimalDB repository;
    @Autowired
    private ScientificClassificationService service;

    /**
     * Takes from Repository the list of all animals present. If this list is Empty,
     * the method throws an Exception.
     *
     * @return Response
     * @throws AnimalException
     */
    public ResponseEntity<Iterable<Animal>> findAll() throws AnimalException {
        Iterable<Animal> animals = repository.findAll();
        ResponseEntity<Iterable<Animal>> response=new ResponseEntity<>(animals, HttpStatus.OK);
        if(repository.count()==0) throw new AnimalException(Errors.NO_ANIMAL_FOUND);
        return response;
    }


    /**
     * Takes from Repository the animal corresponding to the specified id, otherwise the method
     * throws an Exception.
     *
     * @param animalId
     * @return ResponseEntity<Animal>
     * @throws AnimalException
     */
    public ResponseEntity<Optional<Animal>> getAnimalById(int animalId) throws AnimalException{
        Optional<Animal> animal=repository.findById(animalId);
        ResponseEntity<Optional<Animal>> response=new ResponseEntity<>(animal, HttpStatus.OK);
        if(!animal.isPresent()) throw new AnimalException(Errors.NO_ANIMAL_FOUND);
        return response;
    }



    /**
     * Add a new animal in the repository
     * @param animal
     * @return
     * @throws AnimalException
     */
    public ResponseEntity<String>create(Animal animal) throws AnimalException{
        checkIfAnimalAlreadyExists(animal);
        Animal animalToSave = service.checkIfScientificClassExist(animal);
        repository.save(animalToSave);
        return new ResponseEntity<String>("CONFIRM: Operation Done", HttpStatus.OK);
    }



    /**
     * Delete the animal from the repository at the specified id position. If animal to delete doesn't exist it throws an Exception
     * @param id
     * @return
     * @throws AnimalException
     */
    public  ResponseEntity<String> delete(int id) throws AnimalException{
        Optional<Animal> animal=repository.findById(id);
        if(!animal.isPresent()) throw new AnimalException(Errors.NO_ANIMAL_FOUND);
        repository.deleteById(id);
        return new ResponseEntity<String>("CONFIRM: Operation Done", HttpStatus.OK);
    }

    /**
     * Update animal at the specified ID position. If It's not present an animal at the ID position It's throws an Exception
     * @param id
     * @param animal
     * @return
     * @throws AnimalException
     */
    public ResponseEntity<String> update(int id, Animal animal) throws AnimalException{
        if(!repository.findById(id).isPresent()) throw new AnimalException(Errors.NO_ANIMAL_FOUND);
        checkIfAnimalAlreadyExists(animal);
        Animal updatingAnimal=repository.findById(id).get();

        updatingAnimal.setAge(animal.getAge());
        updatingAnimal.setName(animal.getName());
        updatingAnimal.setHabitat(animal.getHabitat());
        updatingAnimal.setSpecies(animal.getSpecies());
        updatingAnimal.setDiet(animal.getDiet());
        updatingAnimal.setNumberOfLegs(animal.getNumberOfLegs());
        updatingAnimal.setScientificClassification(animal.getScientificClassification());
        updatingAnimal.setAnimalId(animal.getAnimalId());

        repository.save(updatingAnimal);
        ResponseEntity<String> response = new ResponseEntity<>("CONFIRM: Operation Done", HttpStatus.OK);
        return response;

    }

    /**
     * It returns the list of animals whose age  is in the specified range [min, max]
     * @param min
     * @param max
     * @return
     */
    public ResponseEntity<Iterable<Animal>> findByAge(int min, int max) {
        List<Animal> list=repository.findAll().stream()
                .filter((b) ->(b.getAge()>min && b.getAge()<=max))
                .collect(Collectors.toList());

        ResponseEntity<Iterable<Animal>> response=new ResponseEntity<>(list, HttpStatus.OK);
        return response;
    }

    /**
     * Get animals with the specified name.
     * @param name
     * @return
     * @throws AnimalException
     */
    public ResponseEntity<Iterable<Animal>> getAnimalByName(String name) throws AnimalException{
        Set<Animal> list=repository.findAllByName(name);
        ResponseEntity<Iterable<Animal>> response=new ResponseEntity<>(list, HttpStatus.OK);
        if(list.isEmpty()) throw new AnimalException(Errors.NO_ANIMAL_FOUND);
        return response;
    }


    /**
     * Get animals with the specified species. If they aren't present it throws an exception
     * @param species
     * @return
     * @throws AnimalException
     */
    public ResponseEntity<Object> getAnimalBySpecies(String species) throws AnimalException {
        checkIfSpeciesPresent(species);
        List<Animal> list = getAllAnimalsBySpecies(species);
        ResponseEntity<Object> response = new ResponseEntity<>(list, HttpStatus.OK);
        return response;
    }



    private List<Animal> getAllAnimalsBySpecies(String species) {

        List<Animal> list = new ArrayList<>();
        for (Animal animals : repository.findAll()) {
            if (species.equals(animals.getSpecies())) {
                list.add(animals);
            }
        }
        return list;
    }


    private void checkIfSpeciesPresent(String species) throws AnimalException {
        for (Animal animals : repository.findAll()) {
            if (species.equals(animals.getSpecies())) {
                return;
            }
        }
        throw new AnimalException(Errors.NO_ANIMAL_FOUND);
    }



    private void checkIfAnimalAlreadyExists(Animal animal) throws AnimalException {
        for (Animal animals : repository.findAll()) {
            if (animal.getName().equals(animals.getName()) &&
                    animal.getSpecies().equals(animals.getSpecies()) &&
                    animal.getHabitat().equals(animals.getHabitat())){

                throw new AnimalException(Errors.ANIMAL_ALREADY_PRESENT);

            }
        }
    }


    public ResponseEntity<String> updateName(int id, String name) throws AnimalException{
        if(!repository.findById(id).isPresent()) throw new AnimalException(Errors.NO_ANIMAL_FOUND);
        Animal animal = repository.findById(id).get();
        checkIfNameIsTheSame(id,name);
        animal.setName(name);
        repository.save(animal);
        ResponseEntity<String> response = new ResponseEntity<>("CONFIRM: Operation Done", HttpStatus.OK);
        return response;

    }

    /**
     * Check if name you want to update is equal to the old one; in this case it throws an exception name_already_exists
     * @param id
     * @param name
     * @throws AnimalException
     */
    private void checkIfNameIsTheSame(int id, String name) throws AnimalException{
        if(repository.findById(id).get().getName().equals(name)) {
            throw new AnimalException(Errors.ANIMAL_ALREADY_PRESENT);
        }

    }

}
