package com.demo.animalfinder.service;


import com.demo.animalfinder.exceptions.AnimalException;
import com.demo.animalfinder.exceptions.Errors;
import com.demo.animalfinder.model.Animal;
import com.demo.animalfinder.model.ScientificClassification;
import com.demo.animalfinder.repository.ScientificClassificationDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ScientificClassificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScientificClassificationService.class);

    @Autowired
    private ScientificClassificationDB repository;


    /**
     * Get all scientific classes from Repository.
     * If list of classes is Empty it throws an Exception(error 106)
     * @return ResponseEntity
     * @throws AnimalException
     */
    public ResponseEntity<Iterable<ScientificClassification>> getAllScientificClass() throws AnimalException {
        Iterable<ScientificClassification> list = repository.findAll();
        ResponseEntity<Iterable<ScientificClassification>> response=new ResponseEntity<>(list, HttpStatus.OK);
        if(repository.count()==0) throw new AnimalException(Errors.NO_SCIENTIFIC_CLASS_FOUND);
        return response;


    }

    /**
     * Get all scientific classes sorted by name and kingdom.
     * If list of Sorted Scientific classes is empty it throws an Exception(errpr 106)
     * @return ResponseEntity
     * @throws AnimalException
     */
    public ResponseEntity<Iterable<ScientificClassification>> getAllSortedScientificClass() throws AnimalException{
        List<ScientificClassification> scientificClasses=repository.findAll();
        ResponseEntity<Iterable<ScientificClassification>> response=new ResponseEntity<>(sortedByScientificClass (scientificClasses), HttpStatus.OK);
        if(repository.count()==0) throw new AnimalException(Errors.NO_SCIENTIFIC_CLASS_FOUND);
        return response;


    }


    /**
     * Add a new Scientific class in the  Repository. If it is already present in  repository
     * there isn't a duplication
     * @param scientificClass
     * @return
     * @throws AnimalException
     */
    public  ResponseEntity<String> addScientificClass(ScientificClassification scientificClassification) throws AnimalException{
        repository.save(scientificClassification);
        return new ResponseEntity<String>("CONFIRM: Operation Done", HttpStatus.OK);

    }

    /**
     * It sorts list of scientific class by name and kingdom
     * @param scientificClasses
     * @return list of sorted scientific Class
     */
    private Iterable<ScientificClassification> sortedByScientificClass(List<ScientificClassification> scientificClasses){
        Comparator<ScientificClassification> compareByScientificClassName = Comparator
                .comparing(ScientificClassification::getScientificClassName)
                .thenComparing(ScientificClassification::getKingdom);

        Iterable<ScientificClassification> sorted = scientificClasses.stream()
                .sorted(compareByScientificClassName)
                .collect(Collectors.toList());

        return sorted;
    }


    public ResponseEntity<Optional<ScientificClassification>> getScientificClassById(int scientificClassificationId) throws AnimalException{
        Optional<ScientificClassification> scientificClass=repository.findById(scientificClassificationId);
        ResponseEntity<Optional<ScientificClassification>> response=new ResponseEntity<>(scientificClass, HttpStatus.OK);
        if(!scientificClass.isPresent()) throw new AnimalException(Errors.NO_ANIMAL_FOUND);
        return response;
    }


    public Animal checkIfScientificClassExist (Animal animal) throws AnimalException{
        ScientificClassification scientificClassification= animal.getScientificClassification();
        for(ScientificClassification c:repository.findAll()) {
            if(c.getScientificClassificationId()==(scientificClassification.getScientificClassificationId())) {
                animal.setScientificClassification(c);
            }
        }
        return animal;
    }
}
