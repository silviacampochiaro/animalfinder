package com.demo.animalfinder.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

    /**
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(value = AnimalException.class)
    public ResponseEntity<ErrorResponse> exception (AnimalException ex) {
        ErrorResponse errorResponse = new ErrorResponse(ex.getCode(), ex.getMessage());
        LOGGER.error("Error");
        return new ResponseEntity<>(errorResponse, ex.getStatus());
    }

}


