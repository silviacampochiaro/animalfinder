package com.demo.animalfinder.exceptions;

import org.springframework.http.HttpStatus;


public enum Errors {

    NO_ANIMAL_FOUND (103, "Animal doesn't exist", HttpStatus.NOT_FOUND),
    NO_SCIENTIFIC_CLASS_FOUND(106, "Scientific class doesn't exist", HttpStatus.NOT_FOUND),
    ANIMAL_ALREADY_PRESENT(107, "Element already present", HttpStatus.CONFLICT),
    BADLY_WRITTEN(108, "Item not supplied correctly", HttpStatus.BAD_REQUEST);

    private final int code;
    private final String message;
    private final HttpStatus status;


    //I define the constructor that I will then use in the classes that use the enum
    Errors (int code, String message, HttpStatus status) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
