package com.demo.animalfinder.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class AnimalException extends Exception {

    private int code;
    private String message;
    private HttpStatus status;

    //step as a parameter to this constructor -> enum, connection via getter in enum
    public AnimalException (Errors error) {
    super();
    this.code = error.getCode();
    this.message = error.getMessage();
    this.status = error.getStatus();

    }


}



