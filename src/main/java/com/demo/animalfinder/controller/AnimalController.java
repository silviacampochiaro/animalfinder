package com.demo.animalfinder.controller;

import com.demo.animalfinder.exceptions.AnimalException;
import com.demo.animalfinder.exceptions.ErrorResponse;
import com.demo.animalfinder.model.Animal;
import com.demo.animalfinder.service.AnimalService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AnimalController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnimalController.class);

    @Autowired
    private AnimalService service;


    /**
     * Get all animals from the Repository
     * @return
     * @throws AnimalException
     */
    @ResponseBody
    @GetMapping("/animals/")
    @ApiOperation(value="Get Animals", notes="Get all animals from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Empty list", response = ErrorResponse.class)
    })
    public ResponseEntity<Iterable<Animal>> getAllAnimals() throws AnimalException {
        LOGGER.info("get all animal");
        return  service.findAll();
    }

    /**
     * Get the selected animal corresponding to the specified ID, the endpoint is built using @PathVariable annotation
     * @param id
     * @return
     * @throws AnimalException
     */
    @GetMapping("/animals/{id}")
    @ApiOperation(value="Get an Animal", notes="Get an animal from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Animal not found", response = ErrorResponse.class)
    })
    public ResponseEntity<Optional<Animal>> getAnimalById(@ApiParam(value=" The Animal corresponding at the specified id", required=true, example="1")
                                                      @PathVariable("id") int id) throws AnimalException {
        return service.getAnimalById(id);
    }


    /**
     * Get the selected animal corresponding to the specified ID, the endpoint is built using @RequestParam annotation
     * @param animalId
     * @return
     * @throws AnimalException
     */
    @GetMapping("/QueryAnimals/")
    @ApiOperation(value="Get an Animal", notes="Get Animal by animalId from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Animal not found", response =ErrorResponse.class)
    })
    public ResponseEntity<Optional<Animal>> getById(@ApiParam(value=" The Animal corresponding at the specified id", required=true, example="1")
                                                  @RequestParam("id") int animalId) throws AnimalException{
        return service.getAnimalById(animalId);
    }




    /**
     * Get animals whose age are in the specified range [min, max]
     * @param min
     * @param max
     * @return
     * @throws AnimalException
     */
    @GetMapping("animals/ageRange/")
    @ApiOperation(value="Get an Animal", notes="Get an animal from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Animal not found", response = ErrorResponse.class)
    })
    public ResponseEntity<Iterable<Animal>> getByAge(@RequestParam("min") int min, @RequestParam("max") int max)  throws AnimalException{
        return service.findByAge(min, max);
    }



    @GetMapping("/animals/{title}")
    public ResponseEntity<Iterable<Animal>> getAnimalByName(@PathVariable("name") String name)  throws AnimalException{
        return service.getAnimalByName(name);
    }

    @GetMapping("/animals/{price}")
    @ApiResponse(code = 400, message = "Animal not found. ", response = ErrorResponse.class)
    @ApiOperation(value = "Get Animal by species", notes = "Get an animal by species")
    public ResponseEntity<Object> getAnimalBySpecies
            (@PathVariable String species) throws AnimalException {
        return service.getAnimalBySpecies(species);
    }


    @PostMapping("/animals")
    @ApiOperation(value="Post Animal", notes="Post an animal in the repository")
    public ResponseEntity<String> create(@RequestBody Animal animal) throws AnimalException {
        return service.create(animal);

    }



    @PutMapping("/update/{id}")
    @ApiOperation(value="Update Animal", notes="Update an animal from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Animal not found", response =ErrorResponse.class),
    })
    public ResponseEntity<String> update(@PathVariable("id") int id, @RequestBody Animal animal) throws AnimalException {
        return service.update(id, animal);
    }



    /**
     * Update name of animal at the specified ID position
     * @param id
     * @param name
     * @return
     * @throws AnimalException
     */
    @PatchMapping("/animal/name/{id}")
    @ApiOperation(value="Update Animal name", notes="Update an animal name from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Animal not found", response = ErrorResponse.class),

    })
    public ResponseEntity<String> updateName(@PathVariable("id") int id, @RequestBody String name) throws AnimalException {
        return service.updateName(id, name);
    }

    @DeleteMapping("animals/{id}")
    @ApiOperation(value="Delete an Animal", notes="Delete an Animal from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Animal to delete not found", response = ErrorResponse.class)
    })
    public void delete(@PathVariable("id") Integer id) throws AnimalException{
       service.delete(id);
    }



}




