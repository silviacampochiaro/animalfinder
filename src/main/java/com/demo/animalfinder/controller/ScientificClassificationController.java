package com.demo.animalfinder.controller;


import com.demo.animalfinder.exceptions.AnimalException;
import com.demo.animalfinder.exceptions.ErrorResponse;
import com.demo.animalfinder.model.ScientificClassification;
import com.demo.animalfinder.service.ScientificClassificationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
public class ScientificClassificationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScientificClassificationController.class);

    @Autowired
    private ScientificClassificationService service;

    /**
     * Get all classes from the repository sorted by class name and kingdom
     * @return
     * @throws AnimalException
     */
    @ResponseBody
    @GetMapping("/scientificClasses/")
    @ApiOperation(value="Get all classes", notes="Get all classes from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 103, message = "Empty list", response = ErrorResponse.class)
    })
    public ResponseEntity<Iterable<ScientificClassification>> getAllScientificClass() throws AnimalException {

        return  service.getAllSortedScientificClass();
    }

    /**
     * Get the selected class corresponding to the specified ID.
     * @param id
     * @return
     * @throws AnimalException
     */
    @GetMapping("/classes/{id}")
    @ApiOperation(value="Get a class", notes="Get class from repository")
    @ApiResponses(value={
            @ApiResponse(code = 200, message = "Ok", response = ErrorResponse.class),
            @ApiResponse(code = 106, message = "Class not found", response = ErrorResponse.class)
    })
    public ResponseEntity<Optional<ScientificClassification>> getAnimalById(@ApiParam(value=" The Class corresponding at the specified id", required=true, example="1")
                                                        @PathVariable("id") int id) throws AnimalException {
        return service.getScientificClassById(id);
    }


    /**
     * Add a scientific class in the  repository
     * @param scientificClassification
     * @return
     * @throws AnimalException
     */
    @PostMapping("/class")
    @ApiOperation(value="Post Scientific Class", notes="Post a Scientific Class in the repository")
    public ResponseEntity<String> create(@RequestBody ScientificClassification scientificClassification) throws AnimalException {
        return service.addScientificClass(scientificClassification);

    }
}
