package com.demo.animalfinder.model;
//describes an animal's parameter


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "animal")
public class Animal {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="sequence")
    int animalId;



    private String name;
    private String habitat;
    private String species;
    private String diet;
    private Long numberOfLegs;
    private int age;


    @ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name ="scientificClassificationId", referencedColumnName = "scientificClassificationId")
    private ScientificClassification scientificClassification;




    public Animal (int animalId,int age,String name, String habitat, String species, String diet, Long numberOfLegs,  ScientificClassification scientificClassification) {
        this.age=age;
        this.name=name;
        this.habitat=habitat;
        this.species=species;
        this.diet=diet;
        this.numberOfLegs=numberOfLegs;
        this.scientificClassification = scientificClassification;
        this.animalId=animalId;
    }


}