package com.demo.animalfinder.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "scientificClassification")
public class ScientificClassification {

    @Id
    int scientificClassificationId;

    private String scientificClassName;
    private String domination;
    private String kingdom;
    private String taxonomy;



    @JsonBackReference
    @OneToMany (mappedBy = "scientificClassification",cascade=CascadeType.PERSIST)
    private List<Animal> animals;



}
