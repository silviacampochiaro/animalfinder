package com.demo.animalfinder.repository;

import com.demo.animalfinder.model.ScientificClassification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientificClassificationDB extends JpaRepository<ScientificClassification, Integer> {





}
