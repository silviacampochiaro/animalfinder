package com.demo.animalfinder.repository;

import com.demo.animalfinder.model.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

//CrudRepository
@Repository
public interface AnimalDB extends JpaRepository<Animal, Integer> {


    Set<Animal> findAllByName (String name);
}
