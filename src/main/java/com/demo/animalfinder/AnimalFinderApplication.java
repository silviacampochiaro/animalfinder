package com.demo.animalfinder;



/*@SpringBootApplication: combines @Configuration (indicates a class
configuration),@ComponentScan (enable scanning and
automatic identification of components / beans)
@EnableAutoConfiguration (automatic creation
 of missing or necessary components / beans)*/

import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@EnableSwagger2
@SpringBootApplication
public class AnimalFinderApplication {


	private static final Logger LOGGER = LoggerFactory.getLogger(AnimalFinderApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AnimalFinderApplication.class, args);
		/*verifica che la connessione sia avvenuta con successo se ci fossero problemi si genererebbe una sqlex.
		 * inserire la creazione di connessioni e statements a livello del blocco try ed è nota
		 * con il nome di try-with-resources. In questo caso non abbiamo più bisogno del blocco finally in
		 *  quanto la chiusura delle risorse utilizzate avverrà automaticamente*/
		String url = "jdbc:mysql://localhost:3306/animals";
		Properties props = new Properties();
		props.setProperty("user", "Igor");
		props.setProperty("password", "Igor");
		try (Connection conn = DriverManager
				.getConnection(url, props);) {
			LOGGER.info("Connection successful");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
//http://localhost:8080/swagger-ui.html#/animal-controller

	/*@Bean: instance a class to return it when required
	(register a Bean)*/
	@Bean
	public Docket swaggerConfiguration(){
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				//specifico il package
				.apis(RequestHandlerSelectors.basePackage("com.demo.animalfinder"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(this.apiInfo())
				.useDefaultResponseMessages(false)
				.protocols(Sets.newHashSet("HTTP"));

	}

	private ApiInfo apiInfo() {
		ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();
		apiInfoBuilder.title("ANIMAL FINDER");
		apiInfoBuilder.description("REST API for an Animal Research");
		apiInfoBuilder.contact(new Contact("Silvia Campochiaro", null, "silvia.campochiaro@spindox.it"));
		apiInfoBuilder.version("1.0.0");
		apiInfoBuilder.license(" GENERAL PUBLIC LICENSE, Version 3");
		apiInfoBuilder.licenseUrl("https://www.gnu.org/licenses/gpl-3.0.en.html");
		return apiInfoBuilder.build();
	}

}
