package com.animalfinder.animalfinder;

import com.demo.animalfinder.exceptions.AnimalException;
import com.demo.animalfinder.exceptions.Errors;
import com.demo.animalfinder.model.Animal;
import com.demo.animalfinder.repository.AnimalDB;
import com.demo.animalfinder.service.AnimalService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoTest {

    @MockBean
    private AnimalDB repository;


    @Autowired
    private AnimalService service;

    private static Animal animal1;
    private static Animal animal2;
    private static Animal animal3;

    @BeforeClass
    public static void setUp() {
        //animal1 = new Animal(1, 3,"Dog", "Pet", "Canis lupus familiaris", 4, "Carnivorous", "Canide");
    }

    @Test
    public void findAllAnimalsWhenItsFull() throws AnimalException {

        List<Animal> list=new ArrayList<>();
        list.add(animal1);


        ResponseEntity<Iterable<Animal>> response=new ResponseEntity<>(list, HttpStatus.OK);

        Mockito.when(repository.findAll()).thenReturn(list);
        Mockito.when(repository.count()).thenReturn((long) 2);
        Assert.assertEquals(service.findAll(), response);
    }


    @Test
    public  void getAllAnimalsWhenEmpty() {

        AnimalException exception=Assert.assertThrows(AnimalException.class, ()->{
           service.findAll();
        });

        Assert.assertEquals(exception.getCode(), Errors.NO_ANIMAL_FOUND.getCode());
    }


    @Test
    public void deleteIfExists() throws AnimalException {
        ResponseEntity<String> response=new ResponseEntity<>("CONFIRM: Operation Done", HttpStatus.OK);

        Mockito.when(repository.findById(1)).thenReturn(Optional.of(animal1));
        Assert.assertEquals(service.delete(1), response);
    }






}
